import { useEffect, useState} from 'react';
import Link from 'next/link';
import Head from 'next/head';
import Image from 'next/image';

import Layout from '../components/layout';

import { faShareAlt, faHeart, faCalculator, faCar, faCamera } from "@fortawesome/free-solid-svg-icons";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Title from '../components/title';
import Card from '../components/cardBox';
import CustomButton from '../components/button';
import CustomTextInput from '../components/input';
import CustomLabel from '../components/label';
import CustomTextArea from '../components/textArea';
import Modal from '../components/modal';
import styles from '../styles/search.module.css';
import Slider from '../components/slider';
import { Creators } from '../services/redux/search/actions';
import ProgressBar from '../components/progressBar';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import store from '../services/redux/store';
import { useRouter } from 'next/router'




const FeatureBox = (props) => (
    <div style={{paddingBottom: 20}}>
        <Title text={props.header} styles={{fontSize: props.Hsize ? props.Hsize : 28, paddingTop: 0}}/>
        <p style={{textTransform: !props.transform ? '' : 'uppercase', fontSize: props.Psize ? props.Psize : 15}}>{props.text}</p>
    </div>
)

const ScrollView = (selector) => (
    document.querySelector(selector).scrollIntoView({behavior: 'smooth'})     
);



const Search = (props) => {
    const [show, showModal] = useState(false);
    const [show2, showModal2] = useState(false);
    const router = useRouter()

    console.log('router.query.search');

    console.log(router.query.search);

    useEffect(() => {
        props.getSearchDetails({address: router.query.search});
        if(props.houses){
            const params = {
                latitude: props.houses[0].latitude,
                longitude: props.houses[0].longitude,
                propertyType: props.houses[0].propertyType,
                squareFootage: props.houses[0].squareFootage,
                bathrooms: props.houses[0].bathrooms,
                address: router.query.search,
                bedrooms: props.houses[0].bedrooms
            }
            const loc = {lat: props.houses[0].latitude, long: props.houses[0].longitude}
            //props.getPriceEstimate(params);
            //props.getSaleEstimate(params);
            //console.log(props.location);
            props.getHouseByLocation(loc);
            (props.location && props.location[0]) && props.getHouseByZillowID({zpid: props.location[0].zpid});
        }

      }, []);    

      console.log("props.sale.price");
      //console.log(props.location[0].zpid);
      console.log(props.location);
      console.log(props.zillow);



    return (
        <Layout
            home={false}
        >
            <div className={styles.subheader}>
                <nav>
                    <ul>
                        <li><a onClick={() => ScrollView('#Overview') }>Overview</a></li>
                        <li><a onClick={() => ScrollView('#Details') }>Details</a></li>
                        <li><a onClick={() => ScrollView('#Schools') }>Schools</a></li>
                        <li><a onClick={() => ScrollView('#Area') }>Area Info</a></li>
                        <li><a onClick={() => ScrollView('#History') }>Property History</a></li>
                    </ul>
                </nav>
                <div className={styles.leftnav}>
                    <CustomButton 
                        value="Request Info or Showing"
                        type="button"
                        btnStyleType="secondary"
                        onClick={() => showModal(!show)}
                    />
                    <button><FontAwesomeIcon icon={faShareAlt} /> <span>share</span></button>
                    <button><FontAwesomeIcon icon={faHeart} /></button>
                </div>
            </div>
            {(props.houses && props.zillow) && <main className={styles.main}>

                <div className={styles.titleBox} id="Overview">
                    <div className={styles.left}>
                        <Title text={props.houses && props.houses[0].addressLine1 ? props.houses[0].addressLine1 : "NULL"} styles={{fontSize: 25, paddingBottom: 7}}/>
                        <h4>{props.houses && props.houses[0].city ? props.houses[0].city : "NULL"}, { props.houses[0].state ? props.houses[0].state : "NULL"} {props.houses[0].zipCode ? props.houses[0].zipCode : "NULL"}</h4>
                    </div>
                    <div className={styles.right}>
                        <Title text={props.zillow && props.zillow.price ? `US$${props.zillow.price}` : "NULL"} styles={{fontSize: 25, paddingBottom: 7}}/>
                        <h4><FontAwesomeIcon icon={faCalculator} /> <span>{props.zillow && props.zillow.rent ? `$${props.price.rent}/MO` : "NULL"}</span></h4>
                    </div>
                </div>

                <div className={styles.pictureBox}>
                    <div className={styles.pictureOne}>
                        <CustomButton 
                            value="Pending"
                            type="button"
                            btnStyleType="ordinary"
                        />
                        <Image 
                            alt=""
                            src="house1.jpeg"
                        />
                        <CustomButton 
                            value="33"
                            type="button"
                            btnStyleType="primary"
                            icon={<FontAwesomeIcon icon={faCamera} />}
                            onClick={() => showModal2(!show2)}
                        />
                    </div>
                    <div className={styles.otherPictures + ' grid grid-flow-col grid-cols-2 grid-rows-2 gap-2'}>
                        <div>
                            <Image 
                                src="house2.jpeg"
                            />
                        </div>
                        <div>
                            <Image 
                                alt=""
                                src="house1.jpeg"
                            />
                        </div>
                        <div>
                            <Image 
                                alt=""
                                src="house2.jpeg"
                            />
                        </div>
                        <div>
                            <Image 
                                alt=""
                                src="house3.jpeg"
                            />
                        </div>
                    </div>
                </div>

                <div className={styles.houseInfoBox}>
                    <div className={styles.info}>
                        <div className='grid grid-flow-col grid-cols-4 grid-rows-2 gap-7 py-7'>
                            <FeatureBox text="BEDROOMS" header={props.houses && props.houses[0].bedrooms ? props.houses[0].bedrooms : "NULL"}/>
                            <FeatureBox text="TOTAL BATHS" header={props.zillow && props.zillow.resoFacts.bathrooms ? props.zillow.resoFacts.bathrooms : "NULL"}/>
                            <FeatureBox text="FULL BATHS" header={props.zillow && props.zillow.resoFacts.bathroomsFull ? props.zillow.resoFacts.bathroomsFull : "NULL"}/>
                            <FeatureBox text="SQUARE FEET" header={props.houses && props.houses[0].squareFootage ? props.houses[0].squareFootage : "NULL"}/>
                            <FeatureBox text="ACRES" header="0.11"/>
                            <FeatureBox text="YEAR BUILT" header={props.zillow && props.zillow.resoFacts.yearBuilt ? props.zillow.resoFacts.yearBuilt : "NULL"}/>
                            <FeatureBox text="ON WEBSITE" header={props.zillow && props.zillow.timeOnZillow ? props.zillow.timeOnZillow : "NULL"}/>
                            <FeatureBox text="STATUS" header={props.zillow && props.zillow.homeStatus ? props.zillow.homeStatus : "NULL"}/>
                        </div>
                        <p>{props.zillow && props.zillow.description ? props.zillow.description : "NULL"}</p>
                    </div>
                    <div className={styles.request}>
                        <Card>
                            <Title text="I&apos;m Interested" styles={{fontSize: 20, textAlign: 'center'}}/>
                            <form>
                                <div>
                                    <CustomLabel text="First name" />
                                    <CustomTextInput 
                                        name="firstname"
                                        placeholder="Enter your first name"
                                    />
                                </div>
                                <div>
                                    <CustomLabel text="Last name" />
                                    <CustomTextInput 
                                        name="lastname"
                                        placeholder="Enter your last name"
                                    />
                                </div>
                                <div>
                                    <CustomLabel text="Email Address" />
                                    <CustomTextInput 
                                        name="email"
                                        placeholder="Enter your Email Address"
                                    />
                                </div>
                                <div>
                                    <CustomLabel text="Phone (optional)" />
                                    <CustomTextInput 
                                        name="phone"
                                        type="tel"
                                        placeholder="Enter your Phone"
                                    />
                                </div>
                                <label className={styles.mybox}>Schedule a showing
                                    <input type="checkbox" />
                                    <span className={styles.checkmark}></span>
                                </label>
                                <div className={styles.radio}>
                                    <span><input type="radio"  /> In Person </span>
                                    <span><input type="radio"  />Virtual </span>
                                </div>
                                <div className="grid grid-cols-2 gap-2 pb-2">
                                    <div>
                                        <CustomLabel text="Date" />
                                        <CustomTextInput 
                                            name="date"
                                            type="date"
                                            placeholder="Enter your Phone"
                                        />
                                    </div>
                                    <div>
                                        <CustomLabel text="Time" />
                                        <CustomTextInput 
                                            name="phone"
                                            type="time"
                                            placeholder="Enter your Phone"
                                        />
                                    </div>
                                </div>
                                
                                <CustomButton 
                                    value="Continue"
                                    type="submit"
                                    btnStyleType="primary"
                                />
                            </form>
                        </Card>
                        <Card>
                            <div className={styles.carTitle}>
                                <FontAwesomeIcon icon={faCar}/>
                                <Title text="My Places" styles={{fontSize: 20, marginTop: -10, marginLeft: 10}}/>
                            </div>  
                            <p><a>Register or sign</a> in to see estimated driving times 
                                between properties and places important to you.
                            </p>                  
                        </Card>
                    </div>
                </div>

                <div className={styles.detailBox} id="Details">
                    <Title text="Details" styles={{fontSize: 28}}/>
                    <div className={styles.innerDetailBox}>
                        <p>INTERIOR</p>
                        <div>
                            <FeatureBox Hsize={17} Psize={13} text="Other" header="Appliances"/>
                            <FeatureBox Hsize={17} Psize={13} text="1" header="Bathrooms Half"/>
                            <FeatureBox Hsize={17} Psize={13} text={props.houses && props.houses[0].bedrooms ? props.houses[0].bedrooms : "NULL"} header="Bedrooms Total"/>
                            <FeatureBox Hsize={17} Psize={13} text={props.houses && props.houses[0].squareFootage ? `${props.houses[0].squareFootage} SqFt.` : "NULL"} header="Living Area"/>
                        </div>
                        <div>
                            <FeatureBox Hsize={17} Psize={13} text={props.houses && props.houses[0].bathrooms ? props.houses[0].bathrooms : "NULL"} header="Bathrooms Full"/>
                            <FeatureBox Hsize={17} Psize={13} text={props.houses && props.houses[0].bathrooms ? props.houses[0].bathrooms : "NULL"} header="Bathrooms Total"/>
                            <FeatureBox Hsize={17} Psize={13} text={props.zillow && props.zillow.resoFacts.flooring[0] ? props.zillow.resoFacts.flooring[0] : "NULL"} header="Flooring"/>
                        </div>
                    </div>
                    <div className={styles.innerDetailBox}>
                        <p>BUILDING AND CONSTRUCTION</p>
                        <div>
                            <FeatureBox Hsize={17} Psize={13} text={props.houses && props.houses[0].propertyType ? props.houses[0].propertyType : "NULL"} header="Property Sub Type"/>
                            <FeatureBox Hsize={17} Psize={13} text={props.zillow && props.zillow.resoFacts.yearBuilt ? props.zillow.resoFacts.yearBuilt : "NULL"} header="Year Built"/>
                        </div>
                        <div>
                            <FeatureBox Hsize={17} Psize={13} text="Residential" header="Property Type"/>
                        </div>
                    </div>
                    <div className={styles.innerDetailBox}>
                        <p>EXTERIOR AND LOT</p>
                        <div>
                            <FeatureBox Hsize={17} Psize={13} text="0.11" header="Lot Size Acres"/>
                            <FeatureBox Hsize={17} Psize={13} text="Square Feet" header="Lot Size Units"/>
                        </div>
                        <div>
                            <FeatureBox Hsize={17} Psize={13} text={props.zillow && props.zillow.resoFacts.lotSize ? props.zillow.resoFacts.lotSize : "NULL"} header="Lot Size Square Feet"/>
                        </div>
                    </div>
                    <div className={styles.innerDetailBox}>
                        <p>UTILITIES</p>
                        <div>
                            <FeatureBox Hsize={17} Psize={13} text={props.zillow && props.zillow.resoFacts.cooling ? props.zillow.resoFacts.cooling : "NULL"} header="Cooling"/>
                        </div>
                        <div>
                            <FeatureBox Hsize={17} Psize={13} text={props.zillow && props.zillow.resoFacts.heating[0] ? props.zillow.resoFacts.heating[0] : "NULL"} header="Heating"/>
                        </div>
                    </div>
                    <div className={styles.innerDetailBox}>
                        <p>AREA AND SCHOOLS</p>
                        <div>
                            <FeatureBox Hsize={17} Psize={13} text={props.houses && props.houses[0].city ? props.houses[0].city : "NULL"} header="City"/>
                            <FeatureBox Hsize={17} Psize={13} text={props.zillow && props.zillow.resoFacts.elementarySchool ? props.zillow.resoFacts.elementarySchool : "NULL"} header="Elementary School"/>
                            <FeatureBox Hsize={17} Psize={13} text={props.zillow && props.zillow.resoFacts.middleOrJuniorSchool ? props.zillow.resoFacts.middleOrJuniorSchool : "NULL"} header="Middle Or Junior School"/>
                        </div>
                        <div>
                            <FeatureBox Hsize={17} Psize={13} text={props.houses && props.houses[0].county ? props.houses[0].county : "NULL"} header="County Or Parish"/>
                            <FeatureBox Hsize={17} Psize={13} text={props.zillow && props.zillow.resoFacts.highSchool ? props.zillow.resoFacts.highSchool : "NULL"} header="High School"/>
                            <FeatureBox Hsize={17} Psize={13} text={props.zillow && props.zillow.resoFacts.cooling ? props.zillow.resoFacts.cooling : "NULL"} header="Subdivision Name"/>
                        </div>
                    </div>
                    <div className={styles.innerDetailBox}>
                        <p>ADDITIONAL INFO</p>
                        <div>
                            <FeatureBox Hsize={17} Psize={13} text={props.zillow && props.zillow.resoFacts.taxAnnualAmount ? `$${props.zillow.resoFacts.taxAnnualAmount}` : 'NULL'} header="Tax Annual Amount"/>
                        </div>
                        <div>
                            <FeatureBox Hsize={17} Psize={13} text="2018" header="Tax Year"/>
                        </div>
                    </div>
                    <div className={styles.innerDetailBox}>
                        <p>AREA AND SCHOOLS</p>
                        <div>
                            <FeatureBox Hsize={17} Psize={13} text="Under Contract" header="Status Detail"/>
                        </div>
                    </div>
                    <div className={styles.company}>
                        <div className={styles.companyInfoTop}>
                            <p><b>Listing By </b> Zillow Homes, Inc.</p>
                            <div>
                            <p><b>MLS# </b>9005614</p>
                            <p><b>Data Last Updated </b>2:09PM - 26/10/2021</p>
                            </div>
                        </div>
                        <div className={styles.companyInfoBottom}>
                            <Image 
                                alt=""
                                src="MLS.png"
                            />                    
                            <p>The data relating to real estate for sale on this website comes in part 
                                from the Internet Data Exchange/ Broker Reciprocity Program of Georgia MLS. 
                                Real estate listings held by brokerage firms other than RE/MAX Advantage are marked with 
                                the Internet Data Exchange/Broker Reciprocity logo and detailed information about them 
                                includes the name of the listing brokers.The broker providing this data believes it to be correct, 
                                but advises interested parties to confirm them before relying on them in a purchase decision. 
                                IDX information is provided exclusively for consumers personal, non-commercial use and may not be 
                                used for any purpose other than to identify prospective properties 
                                consumers may be interested in purchasing. Copyright© 2021 Georgia MLS.
                            </p>
                        </div>
                    </div>
                </div>
                <div className={styles.comparingBox}>
                    <Title text="Comparable Listings" styles={{fontSize: 28}}/>
                    <div>
                    </div>
                </div>
                <div className={styles.schoolBox} id="Schools">
                    <Title text="Schools" styles={{fontSize: 28}}/>
                    <div>
                        <table>
                            <thead>
                                <tr>
                                    <td>NAME</td>
                                    <td>GRADES</td>
                                    <td>STUDENT/ TEACHERS</td>
                                    <td>STATEWIDE RANKING</td>
                                </tr>
                            </thead>
                            <tbody>
                                {(props.zillow && props.zillow.schools) && props.zillow.schools.map((school) => {
                                    <tr>
                                        <td>{school.name ? school.name : "NULL"}</td>
                                        <td>{school.grades ? school.grades : null}</td>
                                        <td>{school.studentsPerTeacher ? `${school.studentsPerTeacher} / 1` : null}</td>
                                        <td>{school.rating ? school.rating : null}</td>
                                    </tr>
                                })}
                            </tbody>
                        </table>
                    </div> 
                </div>
                <div className={styles.statBox} id="Area">
                    <Title text="30315 Statistics and Information" styles={{fontSize: 28}}/>
                    <h4>Data provided by Maponics</h4>

                    <Title text="Transportation" styles={{fontSize: 19, paddingTop: 20}}/>
                    <Card>
                        <div className={styles.transport}>
                            <div>
                                <h4>WALKING</h4>
                                <p><span>2.5/5</span>Somewhat Walkable</p>
                            </div>
                            <div>
                                <h4>DRIVING</h4>
                                <p><span>4.6/5</span>Very Drivable</p>
                            </div>
                        </div>
                    </Card>
                    <Title text="Demographics" styles={{fontSize: 19, paddingTop: 10}}/>
                    <Card>
                        <div className={styles.demo}>
                            <div>
                                <h4>Population By Age</h4>
                                <p><span>2.5/5</span>Somewhat Walkable</p>
                            </div>
                            <div>
                                <Title text='Households With Children' styles={{fontSize: 16, lineHeight: 1}}/>
                                <Title text='48%' styles={{marginTop: 40, paddingBottom: 0, lineHeight: 1}}/>
                            </div>
                            <div>
                                <Title text='Owners' styles={{fontSize: 16, lineHeight: 1}}/>
                                <Title text='34%' styles={{marginTop: 40, paddingBottom: 0, lineHeight: 1}}/>
                            </div>
                            <div>
                                <Title text='Renters' styles={{fontSize: 16, lineHeight: 1}}/>
                                <Title text='66%' styles={{marginTop: 40, paddingBottom: 0, lineHeight: 1}}/>
                            </div>
                        </div>
                    </Card>
                </div>
                <div className={styles.mortgage} >
                    
                    <Title text="Mortgage Calculator" styles={{fontSize: 28}}/>
                    <div className={styles.calculator}>
                        <div className={styles.myprogress}>
                            <ProgressBar />              
                        </div>
                        <div className={styles.form}>
                            <Title text="Estimated Payment: US$5,176/month" styles={{fontSize: 22}}/>
                            <div className={styles.mygrid +` grid grid-cols-4 gap-8`}>
                                <div>
                                    <CustomLabel text="PURCHASE PRICE" />
                                    <CustomTextInput 
                                        name="firstname"
                                    />
                                </div>
                                <div>
                                    <CustomLabel text="INTEREST RATE" />
                                    <CustomTextInput 
                                        name="firstname"
                                    />
                                </div>
                                <div>
                                    <CustomLabel text="DOWN PAYMENT" />
                                    <CustomTextInput 
                                        name="firstname"
                                    />
                                </div>
                                <div>
                                    <CustomLabel text="LOAN TYPE" />
                                    <CustomTextInput 
                                        name="firstname"
                                    />
                                </div>
                            </div>
                            <div className={styles.mygrid +` grid grid-cols-4 gap-8`}>
                                <div>
                                    <CustomLabel text="TAXES (YEARLY)" />
                                    <CustomTextInput 
                                        name="firstname"
                                    />
                                </div>
                                <div>
                                    <CustomLabel text="INSURANCE (YEARLY)" />
                                    <CustomTextInput 
                                        name="firstname"
                                    />
                                </div>
                                <div>
                                    <CustomLabel text="PMI (MONTHLY)" />
                                    <CustomTextInput 
                                        name="firstname"
                                    />
                                </div>
                                <div>
                                    <CustomLabel text="OTHER (MONTHLY)" />
                                    <CustomTextInput 
                                        name="firstname"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <p>Estimated monthly payment amount is based on a default “Purchase Price” as the 
                        list price for the applicable home. Default “Interest Rate” is the national average 5-Year 
                        Adjustable Rate loan rate as of Thursday, 22 July 2021 according to Quandl. 
                        Default “Taxes (Yearly)” is based on NMLS and tax record data based on the property address. 
                        “Down Payment” default is 20% which results in a default “PMI (Monthly)” 
                        of $0. For “Down Payment” of less than 20%, “PMI (monthly)” amount is calculated based on the 
                        “Purchase Price”, “Loan Type” and “Down Payment Amount” and is approximately 2% of US$279,920. 
                        “Insurance (Yearly)” reflects estimated data entered by you. “Other (Monthly)” reflects 
                        estimated data entered by you. Your actual payment may 
                        be higher or lower and all loans are subject to credit approval.
                    </p>
                    <div className={styles.history} id="History">
                        <Title text="Property History" styles={{fontSize: 28}}/>
                        <div>
                            <table>
                                <thead>
                                    <tr>
                                        <td>DATE</td>
                                        <td>SOURCE</td>
                                        <td>DETAILS</td>
                                        <td></td>
                                        <td>CHANGE</td>
                                        <td>PRICE</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    {(props.zillow && props.zillow.priceHistory) && props.zillow.priceHistory.map((history) => {
                                        <tr>
                                            <td>{history.date ? history.date : "NULL"}</td>
                                            <td>{history.source ? history.source : null}</td>
                                            <td>{history.event ? history.event : null}</td>
                                            <td></td>
                                            <td>{history.priceChangeRate ? history.priceChangeRate : null}</td>
                                            <td>{history.price ? history.price : null}</td>
                                        </tr>
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </main>}
            {!props.zillow && <main className={styles.main} style={{height: "100vh"}}>

            </main>}
            {show && <Modal>
                        <CustomButton 
                            value="x"
                            type="button"
                            btnStyleType="primary"
                            styles={{
                                backgroundColor: 'transparent',
                                padding: 0,
                                color: 'grey',
                                marginTop: -15,
                                position: 'absolute',
                                right: 0,
                                
                            }}
                            onClick={() => showModal(!show)}
                        />
                        <Title text="I'm Interested" styles={{fontSize: 20, textAlign: 'center', paddingTop: 0, marginTop: -10}}/>
                    <form className={styles.modalRequest}>
                        <div className="grid grid-cols-2 gap-2">
                            <div>
                                <CustomLabel text="First name" />
                                <CustomTextInput 
                                    name="firstname"
                                    placeholder="Enter your first name"
                                />
                            </div>
                            <div>
                                <CustomLabel text="Last name" />
                                <CustomTextInput 
                                    name="lastname"
                                    placeholder="Enter your last name"
                                />
                            </div>
                        </div>
                        <div className="grid grid-cols-2 gap-2">
                            <div>
                                <CustomLabel text="Email Address" />
                                <CustomTextInput 
                                    name="email"
                                    placeholder="Enter your Email Address"
                                />
                            </div>
                            <div>
                                <CustomLabel text="Phone (optional)" />
                                <CustomTextInput 
                                    name="phone"
                                    placeholder="Enter your Phone"
                                />
                            </div>
                        </div>
                        
                        <label className={styles.mybox}>Schedule a showing
                            <input type="checkbox" />
                            <span className={styles.checkmark}></span>
                        </label>
                        <div className={styles.radio}>
                            <span><input type="radio"  /> In Person </span>
                            <span><input type="radio"  />Virtual </span>
                        </div>
                        
                        <div className="grid grid-cols-2 gap-2">
                            <div>
                                <CustomLabel text="Date" />
                                <CustomTextInput 
                                    name="phone"
                                    placeholder="Enter your Phone"
                                />
                            </div>
                            <div>
                                <CustomLabel text="Time" />
                                <CustomTextInput 
                                    name="phone"
                                    placeholder="Enter your Phone"
                                />
                            </div>
                        </div>
                        <br></br>
                        <div>
                            <CustomTextArea 
                                name="message"
                                value="I am interested in looking at 1798 MEADOW LN SW"
                            />
                        </div>
                        <br></br>
                        <div>
                            <p>By submitting this form, you agree to our Privacy Policy.</p>
                            <CustomButton 
                                value="Submit"
                                type="submit"
                                btnStyleType="primary"
                            />
                        </div>
                    </form>
                </Modal>
            }    

            {show2 && <Slider onClick={() => showModal2(!show2)}/>}
        </Layout>
    )    
}

Search.protoTypes = {
    houses: PropTypes.object.isRequired,
    price: PropTypes.object.isRequired,
    isSearchingHouse: PropTypes.bool.isRequired,
    error: PropTypes.string,
    sale: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    zillow: PropTypes.object.isRequired,

  };
  
  const mapDispatchToProps = dispatch => {
    return {
      getSearchDetails: data => {
        dispatch(Creators.getHouseDetails(data))
      },
      getPriceEstimate: data => {
        dispatch(Creators.getPriceEstimate(data))
      },
      getSaleEstimate: data => {
        dispatch(Creators.getSaleEstimate(data))
      },
      getHouseByLocation: data => {
        dispatch(Creators.getHouseByLocation(data))
      },
      getHouseByZillowID: data => {
        //dispatch(Creators.getHouseByLocation1(data))
      }

    }
  }
  
  const mapStateToProps = (state) => ({
    //price: state.search.price,
    houses: state.search.houses,
    isSearchingHouse: state.search.isSearchingHouse,
    error: state.search.error_message,
    //sale: state.search.sale,
    location: state.search.location,
    zillow: state.search.zillow,


  });
  
  export default connect(mapStateToProps,mapDispatchToProps)(Search);
  
