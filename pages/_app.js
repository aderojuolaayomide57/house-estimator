import '../styles/global.css';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import store from '../services/redux/store';
import { persistor } from '../services/redux/store';



// This default export is required in a new `pages/_app.js` file.
export default function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Component {...pageProps} />
      </PersistGate>
    </Provider>

  )
}
