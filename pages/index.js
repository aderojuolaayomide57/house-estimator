import Link from 'next/link';
import Head from 'next/head';
import Image from 'next/image';

import Layout from '../components/layout';
import Popup from '../components/popup';
import SearchBox from '../components/searchBox';

import styles from '../styles/home.module.css';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useRouter } from 'next/router'






const Home = () => {
  const router = useRouter()
  const formik = useFormik({
    initialValues: {
      search: '',
    },
    validationSchema: Yup.object({
      search: Yup.string()
        .label('Search')
        .required('Enter a valid house address'),
      }),
    onSubmit: (values) => {
      router.push({
        pathname: '/search/',
        query: { search: values.search },

      })
    },

    
  });
  
  return (
    <Layout
      home={true}
      formik={formik}
    >
      <div className="container"></div>
      <div className={styles.houseAd + ' border-b-2 border-white'}>

      </div>
      <div className={styles.houseAd2 + ' border-b-2 border-white'}>
        
      </div>
      <div className={styles.houseAd3 + ' border-b-2 border-white'}>
        
      </div>
    </Layout>
  )  
}

export default Home
