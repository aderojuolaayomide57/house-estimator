import { apiRequest } from '../apiRequests';

import { 
    GET_HOUSE_URL,
    GET_PRICE_ESTIMATE_URL,
    GET_SALE_ESTIMATE_URL,
    GET_HOUSE_BY_LOCATION_URL,
    GET_HOUSE_BY_ZILLOWID_URL
} from '../urls';


export const searchHouse = (params) => {
    return apiRequest("GET", GET_HOUSE_URL, params, false);
}

export const getPriceEstimate = (params) => {
    return apiRequest("GET", GET_PRICE_ESTIMATE_URL, params, false);
}

export const getSaleEstimate = (params) => {
    return apiRequest("GET", GET_SALE_ESTIMATE_URL, params, false);
}

export const getHouseByLocation = (params) => {
    return apiRequest("GET", GET_HOUSE_BY_LOCATION_URL, params, true);
}

export const getHouseByZillow = (params) => {
    return apiRequest("GET", GET_HOUSE_BY_ZILLOWID_URL, params, true);
}
