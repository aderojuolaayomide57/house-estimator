import axios from 'axios';

export const axiosInstance = (switcher) => {
  const instance = axios.create({
    headers: {
      Accept: 'application/json',
      ...({
        "x-rapidapi-key": '004ad17158msh8f47600e4487fa2p1fca4bjsn863e77b18463',
        "x-rapidapi-host": switcher ? 'zillow-com1.p.rapidapi.com' : "realty-mole-property-api.p.rapidapi.com",
      }),
    },
  });

  // maybe we can add an interceptor, which checks if token is expired and refreshes it.
  
  return instance;
};
