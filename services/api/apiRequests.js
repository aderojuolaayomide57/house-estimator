import { axiosInstance } from './axiosDefaults';

export const apiRequest = (method, url, params = {}, switcher) => {
  const response = axiosInstance(switcher)({
    method,
    url,
    params,
  });
  return response;
};