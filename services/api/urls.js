export const BASE_URL = "https://realty-mole-property-api.p.rapidapi.com/";
export const ZILLOW_BASE_URL = "https://zillow-com1.p.rapidapi.com/"

export const GET_HOUSE_URL = `${BASE_URL}properties`;
export const GET_PRICE_ESTIMATE_URL = `${BASE_URL}rentalPrice`;
export const GET_SALE_ESTIMATE_URL = `${BASE_URL}salePrice`;

export const GET_HOUSE_BY_LOCATION_URL = `${ZILLOW_BASE_URL}propertyByCoordinates`;
export const GET_HOUSE_BY_ZILLOWID_URL = `${ZILLOW_BASE_URL}property`;