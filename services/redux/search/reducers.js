import { createReducer } from 'reduxsauce';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import Types from './actionTypes';

export const INITIAL_STATE = {
    isSearchingHouse: false,
    error: false,
    houses: null,
    price: null,
    token: null,
    sale: null,
    location: null,
    zillow: null,
};




export const getHouseDetails = (state = INITIAL_STATE, action) => {
    return {...state, isSearchingHouse: true, error: false};
};

export const getHouseDetailsSucess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSearchingHouse: false,
        error: false,
        houses: action.responseData
    };
};

export const getHouseDetailsFailure = (state = INITIAL_STATE, action) => {
    
    return {
        ...state,
        isSearchingHouse: false,
        error: true,
        error_message: action.error,
    };
};

export const getPriceEstimate = (state = INITIAL_STATE, action) => {
    return {...state, isSearchingHouse: true, error: false};
};

export const getPriceEstimateSucess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSearchingHouse: false,
        error: false,
        price: action.responseData
    };
};

export const getPriceEstimateFailure = (state = INITIAL_STATE, action) => {
    
    return {
        ...state,
        isSearchingHouse: false,
        error: true,
        error_message: action.error,
    };
};

export const getSaleEstimate = (state = INITIAL_STATE, action) => {
    return {...state, isSearchingHouse: true, error: false};
};

export const getSaleEstimateSucess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSearchingHouse: false,
        error: false,
        sale: action.responseData
    };
};

export const getSaleEstimateFailure = (state = INITIAL_STATE, action) => {
    
    return {
        ...state,
        isSearchingHouse: false,
        error: true,
        error_message: action.error,
    };
};

export const getHouseByLocation = (state = INITIAL_STATE, action) => {
    return {...state, isSearchingHouse: true, error: false};
};

export const getHouseByLocationSucess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSearchingHouse: false,
        error: false,
        location: action.responseData
    };
};

export const getHouseByLocationFailure = (state = INITIAL_STATE, action) => {
    
    return {
        ...state,
        isSearchingHouse: false,
        error: true,
        error_message: action.error,
    };
};

export const getHouseByLocation1 = (state = INITIAL_STATE, action) => {
    return {...state, isSearchingHouse: true, error: false};
};

export const getHouseByLocationZillowSucess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSearchingHouse: false,
        error: false,
        zillow: action.responseData
    };
};

export const getHouseByLocationZillowFailure = (state = INITIAL_STATE, action) => {
    
    return {
        ...state,
        isSearchingHouse: false,
        error: true,
        error_message: action.error,
    };
};






export const HANDLERS = {
    
    [Types.GET_HOUSE_DETAILS]: getHouseDetails,
    [Types.GET_HOUSE_DETAILS_SUCCESS]: getHouseDetailsSucess,
    [Types.GET_HOUSE_DETAILS_FAILURE]: getHouseDetailsFailure,

    [Types.GET_PRICE_ESTIMATE]: getPriceEstimate,
    [Types.GET_PRICE_ESTIMATE_SUCCESS]: getPriceEstimateSucess,
    [Types.GET_PRICE_ESTIMATE_FAILURE]: getPriceEstimateFailure,

    [Types.GET_SALE_ESTIMATE]: getSaleEstimate,
    [Types.GET_SALE_ESTIMATE_SUCCESS]: getSaleEstimateSucess,
    [Types.GET_SALE_ESTIMATE_FAILURE]: getSaleEstimateFailure,

    [Types.GET_HOUSE_BY_LOCATION]: getHouseByLocation,
    [Types.GET_HOUSE_BY_LOCATION_SUCCESS]: getHouseByLocationSucess,
    [Types.GET_HOUSE_BY_LOCATION_FAILURE]: getHouseByLocationFailure,

    [Types.GET_HOUSE_BY_LOCATION1]: getHouseByLocation1,
    [Types.GET_HOUSE_BY_LOCATION_ZILLOW_SUCCESS]: getHouseByLocationZillowSucess,
    [Types.GET_HOUSE_BY_LOCATION_ZILLOW_FAILURE]: getHouseByLocationZillowFailure,

  
};

const persistConfig = {
    key: 'search',
    storage: storage,
    blacklist: ['isSearchingHouse']
  };
  
  const SearchReducer = createReducer(INITIAL_STATE, HANDLERS);
  export default persistReducer(persistConfig, SearchReducer)
