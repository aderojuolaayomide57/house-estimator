import { createActions } from 'reduxsauce';

export const { Types, Creators } = createActions(
    {
        getHouseDetails: ['params'],
        getHouseDetailsSuccess: ['responseData'],
        getHouseDetailsFailure: ['error'],

        getPriceEstimate: ['params'],
        getPriceEstimateSuccess: ['responseData'],
        getPriceEstimateFailure: ['error'],
        
        getSaleEstimate: ['params'],
        getSaleEstimateSuccess: ['responseData'],
        getSaleEstimateFailure: ['error'],

        getHouseByLocation: ['params'],
        getHouseByLocationSuccess: ['responseData'],
        getHouseByLocationFailure: ['error'],

        getHouseByLocation1: ['params'],
        getHouseByLocationZillowSucess: ['responseData'],
        getHouseByLocationZillowFailure: ['error'],

    },{},
);