import { all } from 'redux-saga/effects';

import {
    watchHouseDetails,
    //watchPriceEstimate,
    //watchSaleEstimate,
    watchGetHouseByLocation,
    watchGetHouseByZillow,
} from './search';

export default function* rootSaga() {

    yield all([
        watchHouseDetails(),
        //watchPriceEstimate(),
        //watchSaleEstimate(),
        watchGetHouseByLocation(),
        watchGetHouseByZillow(),
    ]);
    
}
