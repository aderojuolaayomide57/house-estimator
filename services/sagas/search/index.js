import { call, put, takeLeading } from 'redux-saga/effects';
import { Creators, Types } from '../../redux/search/actions';
import {
    searchHouse as searchHouseApi,
    getPriceEstimate as getPriceEstimateApi,
    getSaleEstimate as getSaleEstimateApi,
    getHouseByLocation as getHouseByLocationApi,
    getHouseByZillow as getHouseByZillowApi,
} from '../../api/search';
//import { handleError } from '../../../utils/helpers';
import store from '../../redux/store';



export function* getHouseDetails(actions) {
    try {
      const { params } = actions;
      console.log(params);
      const response = yield call(searchHouseApi, {...params});
      yield put(Creators.getHouseDetailsSuccess(response.data));
    } catch (error) {
      //const error_message = handleError(error)
      console.log(error)
      yield put(Creators.getHouseDetailsFailure(error));
    }
  }
  
export function* watchHouseDetails() {
  yield takeLeading(Types.GET_HOUSE_DETAILS, getHouseDetails);
}

export function* getPriceEstimate(actions) {
  try {
    const { params } = actions;
    console.log(params);
    const response = yield call(getPriceEstimateApi, {...params});
    yield put(Creators.getPriceEstimateSuccess(response.data));
  } catch (error) {
    //const error_message = handleError(error)
    console.log(error)
    yield put(Creators.getPriceEstimateFailure(error));
  }
}

export function* watchPriceEstimate() {
  yield takeLeading(Types.GET_PRICE_ESTIMATE, getPriceEstimate);
}

export function* getSaleEstimate(actions) {
  try {
    const { params } = actions;
    console.log(params);
    const response = yield call(getSaleEstimateApi, {...params});
    yield put(Creators.getSaleEstimateSuccess(response.data));
  } catch (error) {
    //const error_message = handleError(error)
    console.log(error)
    yield put(Creators.getSaleEstimateFailure(error));
  }
}

export function* watchSaleEstimate() {
  yield takeLeading(Types.GET_SALE_ESTIMATE, getSaleEstimate);
}

export function* getHouseByLocation(actions) {
  try {
    const { params } = actions;
    console.log(params);
    const response = yield call(getHouseByLocationApi, {...params});
    yield put(Creators.getHouseByLocationSuccess(response.data));
  } catch (error) {
    //const error_message = handleError(error)
    console.log(error)
    yield put(Creators.getHouseByLocationFailure(error));
  }
}

export function* watchGetHouseByLocation() {
  yield takeLeading(Types.GET_HOUSE_BY_LOCATION, getHouseByLocation);
}

export function* getHouseByZillow(actions) {
  try {
    const { params } = actions;
    console.log(params);
    const response = yield call(getHouseByZillowApi, {...params});
    yield put(Creators.getHouseByLocationZillowSucess(response.data));
  } catch (error) {
    //const error_message = handleError(error)
    console.log(error)
    yield put(Creators.getHouseByLocationZillowFailure(error));
  }
}

export function* watchGetHouseByZillow() {
  yield takeLeading(Types.GET_HOUSE_BY_LOCATION1, getHouseByZillow);
}



