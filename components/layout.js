import { useEffect, useState, useRef } from 'react';
import Link from 'next/link';
import Head from 'next/head';
import Image from 'next/image';
import { faSearch, } from "@fortawesome/free-solid-svg-icons";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import AcceptCookie from './acceptCookie';
import styles from '../styles/layout.module.css';



const Layout = ({children, home, formik}) => {
    const [show, showInput] = useState(false);
    const wrapperRef = useRef(null);


    useEffect(() => {
        function handleClickOutside(event) {
            if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
                if (document.forms["search"]["search"].value === "")
                    showInput(!show)
            }
        }
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [wrapperRef,show]);
    
    return (
        <div className="" style={{position: 'relative'}}>
            <Head>
                <title>Light Echo</title>
                <meta name="description" content="Light Echo" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            {home && <div className={styles.hearderBgImage}>
                <header className="header-box flex justify-start md:justify-start border-t-4 border-blue-900 sticky h-16 bg-white z-10">
                    <h1 className="justify-around text-xl font-medium p-4">
                        <span className="text-blue-900">Light</span> Echo
                    </h1>
                </header>
                <div className={styles.headercontent}>
                    <div className={styles.colorMix + ' ' + styles.left}>
                        <div className={styles.innerColorMix}></div>
                    </div>
                    <div className={styles.colorMix + ' ' + styles.right}>
                    <div className={styles.innerColorMix2}></div>
                    </div>
                    <div className={styles.searchBox}>
                        <h1>The Right Agent Every Step of the Way</h1>
                        <form name="search" className={styles.innerSearchBox} onSubmit={formik.handleSubmit}
                            ref={wrapperRef}
                        >
                            { show && 
                                <input type="text" name="search" 
                                    onChange={formik.handleChange}
                                    value={formik.values.search}
                                />
                            }
                            { !show && <div className={styles.placeholder} onClick={() => showInput({show: true}) }>
                                <p>Address, City, ZIP, and <span>More</span></p>
                            </div>}
                            <button type="submit"><FontAwesomeIcon icon={faSearch} /></button>
                        </form>
                        <p>LISTING DATA LAST UPDATED TODAY AT 11:06</p>
                    </div>
                </div>
            </div>}
            {!home && <header className={ styles.headerbox + ` flex justify-start md:justify-start border-t-4 border-blue-900 sticky h-16 bg-white z-10`}>
                    <h1 className="justify-around text-xl font-medium p-4">
                        <span className="text-blue-900">Light</span> Echo
                    </h1>
                </header>
            }
            <div className="z-10" style={{backgroundColor: '#f4f4f4'}}>
                {children}
            </div>
            <footer className={styles.footer}>
                <div className="top grid grid-cols-1 sm:grid-cols-5 gap-3">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
                <div className={styles.bottom + ' border-t border-white'}>
                    <div className={styles.info}>
                        <p>This is a test and free website for house evaluation<br/>
                        Feel free to use and explore, if you need any help you can reach out 
                        to us or if you need something like this.<br/>

                        © 2021 LIGHT ECHO, LLC. All Rights Reserved. Powered by lex.
                        </p>
                    </div>
                    <div className={styles.social}>
                        <div className={styles.socialTop}>
                            <button ><FontAwesomeIcon icon={faSearch} /></button>
                            <button ><FontAwesomeIcon icon={faSearch} /></button>
                            <button ><FontAwesomeIcon icon={faSearch} /></button>
                            <button ><FontAwesomeIcon icon={faSearch} /></button>
                        </div>
                        <div className={styles.socialBottom}>
                            <Link href="/"><a>PRIVACY POLICY</a></Link>  
                            <Link href="/"><a>TERM OF USE</a></Link>                                              
                        </div>
                    </div>
                </div>
            </footer>
            <AcceptCookie />
        </div>
    )

}

export default Layout;