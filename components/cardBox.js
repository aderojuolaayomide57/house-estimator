import Link from 'next/link';
import Head from 'next/head';
import Image from 'next/image';


import styles from '../styles/card.module.css';



const Card = ({children, style}) => {

    return (
        
        <div className={styles.card + ` shadow p-4 bg-white rounded-sm mb-6`} style={style}>
            {children}
        </div>
    )

}

export default Card;