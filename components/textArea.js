import PropTypes from 'prop-types';
import styles from '../styles/textarea.module.css';


const CustomTextArea = (props) => (
    
    <textarea 
        name={props.name}
        placeholder={props.placeholder}
        onChange={props.onChange}
        value={props.value}
        style={props.style}
        className={styles.textarea}
    />
)

CustomTextArea.propTypes = {
    focus: PropTypes.bool.isRequired,
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string,
    style: PropTypes.shape({}),
}

CustomTextArea.defaultProps = {
    focus: false,
    placeholder: null,
    style: null,
}

export default CustomTextArea;