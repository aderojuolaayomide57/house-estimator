import React, { useState, useEffect, useRef } from 'react'

import SliderContent from './SliderContent';
import Slide from './Slide';
import Arrow from './Arrow';



/**
 * @function Slider
 */


const sliderCss = {
    position: "relative",
    height: "100vh",
    width: "700px",
    margin: '0 auto',
    overflow: 'hidden' 
}

  



const NewSlider = ({translate,transition,images,width,len}) => {
    //let mywidth = 1233

    console.log(images)

   return (
        <div style={sliderCss}>
            <SliderContent
                translate={translate}
                transition={transition}
                width={width * len}
            >
                {images && images.map((slide, i) => (
                    <Slide key={slide + i} content={slide} />
                ))}
            </SliderContent>
        </div>
    )
}

export default NewSlider
