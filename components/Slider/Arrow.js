import React from 'react';
import Image from 'next/image';
import { faShareAlt, faHeart, faArrowLeft, faAngleLeft, faAngleRight} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


const ImgBox = ({direction}) => {
  const css =  {transform: translateX(`${direction === 'left' ? '-2' : '2'}px`)}
  

  return (
    direction === 'right' ? <Image alt="" style={css} src={rightArrow} /> : <Image alt="" style={css} src={leftArrow} />
  )


}
const Arrow = ({ direction, handleClick }) => (
    <div
      onClick={handleClick}
      style={{
        display: 'flex',
        position: 'absolute',
        top: '30%',
        right: direction === 'right' && -25,
        left: direction === 'left' && -25,
        height: 50,
        width: 50,
        justifyContent: 'center',
        background: 'red',
        borderRadius: '50%',
        cursor: 'pointer',
        alignItems: 'center',
        transition: 'transform ease-in 0.1s',
      }}
    >
      <button><FontAwesomeIcon icon={direction === 'right' ? faAngleRight : faAngleLeft} /></button>
    </div>
  )
  
  export default Arrow
  