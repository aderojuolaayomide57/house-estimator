import React from 'react'

const Slide = ({ content }) => (
  <div
    style={{
      height: "100%",
      width: "100vw",
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      backgroundImage: `url(${content})`
    }}
  />
)

export default Slide
