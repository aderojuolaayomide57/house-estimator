import Link from 'next/link';
import Head from 'next/head';
import Image from 'next/image';


import styles from '../styles/label.module.css';



const CustomLabel = (props) => {
    return (
        
        <label className={styles.label} style={props.styles}>
            {props.text}
        </label>
    )

}

export default CustomLabel;