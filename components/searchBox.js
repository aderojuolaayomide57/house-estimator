import Link from 'next/link';
import Head from 'next/head';
import Image from 'next/image';


import styles from '../styles/search.module.css';




const SearchBox = () => {

    return (
        
        <div className="md:container md:mx-auto md:w-auto rounded-md shadow-md">
            <div className="flex justify-between">
                <div className="flex justify-start">
                    <select className="px-2 py-2 w-16 border-0">
                        <option>All</option>
                    </select>
                </div>
                <div className="justify-end">
                    <select className="px-2 py-2 border-0 w-32">
                        <option>Immediate</option>
                    </select>
                </div>
            </div>
            <div className="flex">
                <div className="">
                    <p>From</p>
                    <select className="form-group px-2 py-2 w-40 border-0 rounded">
                        <option>All</option>
                    </select>
                </div>
                <div className="">
                    <p>To</p>
                    <select className="form-group px-2 py-2 border-0 w-40 rounded">
                        <option>Immediate</option>
                    </select>
                </div>
            </div>

            
        </div>
    )

}

export default SearchBox;