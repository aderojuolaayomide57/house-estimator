import Link from 'next/link';
import Head from 'next/head';
import Image from 'next/image';


import styles from '../styles/popup.module.css';
import { Spring, animated } from 'react-spring'




const Popup = () => {

    return (
        
        <div className={styles.popup}>
            <Image alt="" className={styles.image} src="CLPX.200x200.png"/>
            <div className={styles.textbox}>
                <p>Customer sent 1 BTCLN <br/>3 hours ago. </p>
                <span>x</span>
            </div>
        </div>
    )

}

export default Popup;