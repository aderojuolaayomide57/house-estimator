import Link from 'next/link';
import Head from 'next/head';
import Image from 'next/image';


import styles from '../styles/layout.module.css';



const Title = (props) => {

    return (
        
        <h1 className={styles.title} style={props.styles}>
            {props.text}
        </h1>
    )

}

export default Title;