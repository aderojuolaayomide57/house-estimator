
import Link from 'next/link';
import Image from 'next/image';

import Modal from '../components/modal';
import { faShareAlt, faHeart, faArrowLeft, faAngleLeft, faAngleRight} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from '../styles/slider.module.css';
import Title from '../components/title';
import CustomButton from '../components/button';
import NewSlider from '../components/Slider/index';

import React, { useState, useEffect, useRef } from 'react'



const Slider = (props) => {
    const val = true
    const getWidth = () => window.innerWidth;
    //console.log(getWidth())

    const images = [
        'https://images.unsplash.com/photo-1449034446853-66c86144b0ad?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2100&q=80',
        'house1.jpeg',
        'house2.jpeg',
        'house3.jpeg'
    ];


    const image = [1,2,4]

    //console.log(images[0])

    



    const [ gallery, showGallery] = useState(false)

    const [state, setState] = useState({
        activeIndex: 0,

        translate: 0,
        transition: 0.45
    })
    
    const { translate, transition, activeIndex } = state

    const nextSlide = () => {
        if (activeIndex === images.length - 1) {
          return setState({
            ...state,
            translate: 0,
            activeIndex: 0
          })
        }
    
        setState({
          ...state,
          activeIndex: activeIndex + 1,
          translate: (activeIndex + 1) * getWidth()
        })
      }
    
      const prevSlide = () => {
        if (activeIndex === 0) {
          return setState({
            ...state,
            translate: (images.length - 1) * getWidth(),
            activeIndex: images.length - 1
          })
        }
    
        setState({
          ...state,
          activeIndex: activeIndex - 1,
          translate: (activeIndex - 1) * getWidth()
        })
      }
    
  
    return (
        <Modal 
            mystyle={{width: "90%", borderRadius: 5, padding: 0, height: "90%", overflow: 'hidden'}}
        >
            <div className={styles.sliderHeader}>
                <div className={styles.rightnav}>
                    <Title 
                        text="1798 MEADOW LN SW, Atlanta, GA 30315"
                        styles={{fontSize: 15, paddingBottom: 0, lineHeight: 1}}
                    />
                    <p>Zillow Homes, Inc.</p>
                </div>
                <div className={styles.leftnav}>
                <button><FontAwesomeIcon icon={faHeart} /></button>
                <button><FontAwesomeIcon icon={faShareAlt} /></button>

                    <CustomButton 
                        value="Request Info or Showing"
                        type="button"
                        btnStyleType="secondary"
                    />
                    <button onClick={() => props.onClick()}>x</button>
                </div>
            </div>
            <div className={styles.sliderBody}>
                <div className={styles.gallery} style={gallery ? { width: 'auto' } : {}}>
                    <div className="grid grid-rows grid-flow-row gap-2 h-screen" style={gallery ? { display: 'none' } : {}}>
                        <div className="row-auto">
                            <Image 
                                alt=""
                                src="house2.jpeg"
                            />
                        </div>
                        <div className="row-auto">
                            <Image 
                                alt=""
                                src="house1.jpeg"
                            />
                        </div>
                        <div className="row-auto">
                            <Image 
                                alt=""
                                src="house2.jpeg"
                            />
                        </div>
                        <div className="row-auto">
                            <Image 
                                alt=""
                                src="house3.jpeg"
                            />
                        </div>
                        <div className="row-auto">
                            <Image 
                                alt=""
                                src="house3.jpeg"
                            />
                        </div>
                        <div className="row-auto">
                            <Image 
                                alt=""
                                src="house3.jpeg"
                            />
                        </div>
                        <div className="row-auto">
                            <Image 
                                alt=""
                                src="house3.jpeg"
                            />
                        </div>
                        <div className="row-auto">
                            <Image 
                                alt=""
                                src="house2.jpeg"
                            />
                        </div>
                        <div className="row-auto">
                            <Image 
                                alt=""
                                src="house3.jpeg"
                            />
                        </div>
                        <div className="row-auto">
                            <Image 
                                alt=""
                                src="house3.jpeg"
                            />
                        </div>
                        <br></br>
                    </div>
                </div>
                <div className={styles.slider} style={gallery ? { width: 'auto' } : {}}>
                    <button className={styles.hide} onClick={() => showGallery(!gallery)}><FontAwesomeIcon icon={faArrowLeft} /></button>
                    <div className={styles.innerBox} style={gallery ? {paddingTop: 0, marginTop: -10, marginBottom: 0, marginLeft: 50 }: {}}>
                        <span onClick={() => prevSlide()}><button><FontAwesomeIcon icon={faAngleLeft} /></button></span>
                        <div className={styles.img} style={gallery ? {width: "inherit", marginLeft: 80} : {}}>
                            <NewSlider
                                width={getWidth()}
                                translate={translate}
                                transition={transition}
                                activeIndex={activeIndex}
                                images={images}
                                len={images.length}
                            />
                        </div>
                        <span onClick={() => nextSlide()} style={gallery ? { right: "-5vw" } : {}}><button>
                                <FontAwesomeIcon icon={faAngleRight} /></button>
                        </span>
                    </div>
                    <div className={styles.sliderBottom} style={gallery ? { marginTop: -10 } : {}}><p>1 of 33 Photos</p></div>
                </div>
            </div>

        </Modal>
    )
}


export default Slider;