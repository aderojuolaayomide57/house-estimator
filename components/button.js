import Link from 'next/link';
import PropTypes from 'prop-types';


import styles from '../styles/button.module.css';



const CustomButton = (props) => {


    let selectedButton;
    if (props.btnStyleType === "secondary") {
        selectedButton = styles.secondary;

    }else if(props.btnStyleType === 'ordinary') {
        selectedButton = styles.ordinary;
    }else{
        selectedButton = styles.primary;
    }

    return (
        
        <button 
            type={props.type}
            className={props.disabled ? styles.disabled : selectedButton}
            style={props.styles}
            onClick={props.onClick ? () => props.onClick() : null }
            disabled={props.disabled}
        >
             {props.icon && <span>{props.icon}</span>} {props.value && props.value}
        </button> 
    )

}

CustomButton.propTypes = {
    type: PropTypes.string,
    style: PropTypes.shape({}),
    value: PropTypes.string,
    btnStyleType: PropTypes.string.isRequired,
    icon: PropTypes.shape({}),
    onClick: PropTypes.func,
    disabled: PropTypes.bool.isRequired,
}

CustomButton.defaultProps = {
    value: null,
    type: 'submit',
    style: null,
    btnStyleType: "primary",
    icon: null,
    onClick: null,
    disabled: false
}


export default CustomButton;