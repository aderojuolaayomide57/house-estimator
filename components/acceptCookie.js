import Link from 'next/link';
import Head from 'next/head';
import Image from 'next/image';


import styles from '../styles/acceptcookie.module.css';
import { Spring, animated } from 'react-spring';
import Title from '../components/title';
import CustomButton from './button';




const AcceptCookie = () => {

    return (
        
        <div className={styles.cookie}>
            <div>
                <Title text="Our site uses cookies"/>
                <p>This site uses cookies and related technologies for site operation, analytics, and third party 
                    advertising purposes as described in our <a>Privacy Notice</a>.</p>
            </div>
            <CustomButton 
                value="Accept"
                type="submit"
                btnStyleType="primary"
            />            
        </div>
    )

}

export default AcceptCookie;