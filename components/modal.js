import styles from '../styles/modal.module.css';


const Modal = ({children, mystyle}) => {

    return(
        <div className={styles.modal}>

            <div className={styles.modalContent} style={mystyle}>
                {children}
            </div>

        </div>
    )

}


export default Modal; 